## This module implements constants, types, and helpers common to all
## modules.

import math


const
  FRAMES_PER_SEC* = 16_000
    ## Sampling rate for, e.g., recording and created WAV files.
  MS_PER_SEC* = 1_000
    ## Number of milliseconds within one second.
  FRAMES_PER_MS* = FRAMES_PER_SEC div MS_PER_SEC
    ## Number of frames within one second.
  NUM_CHANNELS* = 1
    ## Number of channels for, e.g., recording and created WAV files.
  SAMPLE_SILENCE* = 0.0
    ## Sample value which represents silence.
  SAMPLE_MAX_AMPLITUDE* = 1.0
    ## Full-scale sample value.

  # see bottom of file for more constants

assert FRAMES_PER_SEC mod MS_PER_SEC == 0

type
  FrameIndex* = uint
    ## An index representing a specific frame in, e.g., a buffer.
  FrameCount* = uint
    ## A counter representing a number of frames in, e.g., a buffer.
  ChannelCount* = uint
    ## A counter representing a number of channels in, e.g., a frame.
  SampleValue* = float
    ## The value of a single sample.
  SecondValue* = float
    ## A duration seconds.
  FrameData* = array[NUM_CHANNELS, SampleValue]
    ## A sample per channel for one sampling point.


proc `+`*(a: FrameData; b: FrameData): FrameData =
  ## Adds frame ``a`` and ``b``, i.e., mixing the two signals together.
  for i in 0..<a.len:
    result[i] = a[i] + b[i]


proc `-=`*(a: var FrameData; b: FrameData) =
  ## Subtracts frame ``b`` from frame ``a``. Used for filtering, e.g.,
  ## to remove an unwanted filtered version of a signal from itself.
  for i in 0..<a.len:
    a[i] -= b[i]


proc `*`*(f: FrameData; gain: float): FrameData =
  ## Gains a frame by a certain amount.
  for i in 0..<f.len:
    result[i] = f[i] * gain


proc silence*(frame: var FrameData) =
  ## Mutes ``frame``, i.e., sets its samples to silence.
  for i in 0..<frame.len:
    frame[i] = SAMPLE_SILENCE


proc dbfsToAmplitude*(db: float): float =
  ## Converts a dB value to the corresponding amplitude value
  ##  (e.g., ``-1 dB`` to ``0.89125…``). Reverse of amplitudeToDbfs_.
  pow(10, db / 20.0)


proc amplitudeToDbfs*(coeff: cfloat): cfloat =
  ## Converts a amplitude value to the corresponding dB value
  ##  (e.g., ``0.89125…`` to ``-1 dB``). Reverse of dbfsToAmplitude_.
  20 * log10(coeff)


proc msToSamples*(ms: float): FrameCount =
  ## Returns the approximate number of frames within ``ms``
  ## milliseconds.
  FrameCount(round(FRAMES_PER_MS * ms))

iterator frameIndices*(count: FrameCount): FrameIndex =
  ## Iterates from frame index zero to excluding ``count`` using the
  ## the custom type (alias) for frame indices.
  var fi = FrameIndex(0)
  while fi < count:
    yield fi
    fi += 1

const
  BITS_PER_SAMPLE* = sizeof(SampleValue) * 8
    ## Number of bits required to store one sample.
  FRAME_SILENCE* = block:
    ## A frame representing silence on all channels.
    var frame: FrameData
    silence(frame)
    frame

