.. image:: https://gitlab.com/hpi-potsdam/osm/flexidug/radioio/badges/main/pipeline.svg
  :target: https://gitlab.com/hpi-potsdam/osm/flexidug/radioio/pipelines
  :align: right

=======
radioio
=======

In the context of the `FlexiDug <https://osm.hpi.de/flexidug/>`__
project, this program is the glue between the APIs of the *Zugleiter in
the Cloud* (ZliC) and voice radio hardware.

Activated by voice, this program will record and upload an audio file
via a WebSocket. Audio files which are returned via the WebSocket are
played back.

The radio hardware (e.g., a PMR radio) must be wired up with the default
sound devices of the computer which runs this program. Unsurprisingly,
the computer's audio output must be wired to the radio's input and the
computers' input must be wired to the radio's output. See also wiring_.

running
-------

prerequisites:

- `PortAudio <http://www.portaudio.com/>`__ library

You can
`download the latest debug build here
<https://gitlab.com/hpi-potsdam/osm/flexidug/radioio/-/jobs/artifacts/main/raw/build/radioio?job=debug>`__
and
`download the latest release build here
<https://gitlab.com/hpi-potsdam/osm/flexidug/radioio/-/jobs/artifacts/main/raw/build/radioio?job=release>`__.

development
-----------

prerequisites:

- `nim <https://nim-lang.org/>`__
- `PortAudio <http://www.portaudio.com/>`__ headers

installation of dependencies::

  nimble install -d

debug build::

  nimble build

release build::

  nimble build -d:release

cleaning::

  nimble clean

Docker
......

Build in Docker image:

.. code:: shell

  docker build -t radioio .

On Windows, start PulseAudio in Bash:

.. code:: shell

  /path/to/pulseaudio-1.1/bin/pulseaudio.exe

Run Docker image:

.. code:: shell

  docker run -it --rm radioio

If required, manually test audio in Docker:

.. code:: shell

  docker run -it --rm debian bash
  export PULSE_SERVER=tcp:host.docker.internal:4713
  apt-get update
  apt-get install pulseaudio-utils
  parecord test1.wav
  paplay test1.wav

code docs
---------

the development `documentation can be found here
<https://hpi-potsdam.gitlab.io/osm/flexidug/radioio/radioio.html>`__

wiring
------

for Midland plug to TRRS (3.5 mm headset plug):

.. list-table::

  - - ↓ Midland / TRRS →
    - tip (left)
    - ring (right)
    - ring (ground)
    - sleeve (mic)

  - - | 3.5 mm tip
      | (speaker)
    -
    -
    -
    - x

  - - | 3.5 mm ring
      | (none)
    -
    -
    -
    -

  - - | 3.5 mm sleeve
      | (ground)
    -
    -
    - x
    -

  - - | 2.5 mm tip
      | (mic)
    - 10 kΩ
    - 10 kΩ
    -
    -

  - - | 2.5 mm ring
      | (DC & data)
    -
    -
    -
    -

  - - | 2.5 mm sleeve
      | (ground)
    -
    -
    - x
    -
