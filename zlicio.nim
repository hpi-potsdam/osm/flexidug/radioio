## This module is responsible for input/output to/from the ZliC.
## This mainly includes managing and using a WebSocket connection.

import std/tempfiles

import asyncdispatch
import logging
import os
import uri

import ws
import uuid4


type
  ZlicIoData* = object
    ## State to use and manage a connection to the ZliC.
    url: string
    ## The address under which the ZliC has been contacted.
    ws: WebSocket
    ## The handle of the opened WebSocket.
    pingInterval: int
    ## Time between keepalive messages to be send over the WebSocket.
    retries: seq[int]
    ## Amounts of seconds to wait between failed communication.
    ## For (re)connect attempts, the last value will be used to retry
    ## forever if all previous attempts failed, because this program
    ## cannot do anything useful otherwise anyway.
    ## In contrast, transmissions will be discarded after all retries
    ## failed.
    uuid*: string
    ## Unique identifier for a client. The value is initially generated
    ## and then stored/recalled from a configuration file on disk.

proc ready(zio: ZlicIoData): bool =
  ## Returns whether the connection to the ZliC is ready to be used.
  if zio.ws.isNil:
    return false
  if zio.ws.readyState != ReadyState.Open:
    return false
  return true


proc sendRetried(zio: var ZlicIoData; text: string, opcode: Opcode) =
  ## Sends `text` and `opcode` via the WebSocket.
  ##
  ## The WebSocket needs to be opened (and still ready) before calling
  ## this procedure. If the transfer fails, however, this procedure
  ## retries according to configuration and logs an error if all
  ## attempts fail.
  ##
  ## This is the code where messages actually leave our implementation.
  for i, ms in zio.retries:
    try:
      waitFor zio.ws.send(text, opcode)
      break
    except Exception as e:
      warn("sending via WebSocket failed (try ", i, "): ", e.msg)
      warn("retrying to send in ", ms, " ms")
      sleep(ms)

  error("All retry attempts failed. Giving on this message.")


proc sendBinary(zio: var ZlicIoData; binary: string) =
  ## Shortcut to send binary data retried, see sendRetried_.
  sendRetried(zio, binary, Opcode.Binary)


proc ensureConnection(zio: var ZlicIoData) =
  ## Checks if the connection to the ZliC is ready, if not, tries to
  ## reconnect forever.
  var retryIndex = 0

  while true:

    let ms = zio.retries[retryIndex]

    if zio.ready:
      return

    debug("opening WebSocket to ", zio.url)
    try:
      zio.ws = waitFor newWebSocket(zio.url)
      zio.ws.setupPings(float(zio.pingInterval))
      info("connected to WebSocket server ", zio.url)
      continue
    except Exception as e:
      warn("connection to ", zio.url, " failed: ", e.msg)

    warn("retrying to connect in ", ms, " ms")
    sleep(ms)

    if retryIndex < zio.retries.len - 1:
      retryIndex += 1


proc initZlicIo*(zio: var ZlicIoData; hostname: string; port: int;
                 basepath: string, pingInterval = 9;
                 retries = @[0, 1, 3, 10]) =
  ## Sets up a input/output connection (WebSocket) to the ZliC.
  ##
  ## `retries` are specified in seconds and used as timeout between
  ## connect/reconnect/send attempts. Setup logic includes the loading
  ## or generation and storage of client UUID.

  zio.pingInterval = pingInterval

  for secs in retries:
    zio.retries.add(secs * 1_000)

  let configDir = getConfigDir()/"radioio"
  if not dirExists(configDir):
    debug("creating configuration directory")
    createDir(configDir)

  let uuidFile = configDir / "uuid"
  if not fileExists(uuidFile):
    debug("creating UUID file")
    zio.uuid = $(uuid4())
    let f = open(uuidFile, fmWrite)
    defer: f.close()
    f.writeLine(zio.uuid)
  else:
    debug("reading UUID file")
    let f = open(uuidFile)
    defer: f.close()
    zio.uuid = f.readLine()

  block:
    var uri = initUri()
    uri.scheme = "ws"
    uri.hostname = hostname
    uri.port = $port
    uri.path = $(parseUri(basepath)/zio.uuid)
    zio.url = $uri


proc upload*(zio: var ZlicIoData; fileName: string) =
  ## Sends a file via the WebSocket, (re)connects if required.
  debug("uploading ", fileName)

  zio.ensureConnection

  # Load whole file into memory? Honestly? WebSocket library does not
  # give us another way to submit data for sending, I guess.
  zio.sendBinary(readFile(fileName))


proc download*(zio: var ZlicIoData): string =
  ## Waits for data being received via the WebSocket. After some sanity
  ## checks, data is written to a temporary file. On success, the path
  ## to the temporary file is returned. On failure, debug/warn/error
  ## messages are logged *and another attempt to receive data is made*.
  var data: seq[byte]
  while true:
    zio.ensureConnection
    try:
      data = waitFor zio.ws.receiveBinaryPacket()
    except WebSocketPacketTypeError:
      debug("ignoring non-binary WebSocket packet")
      continue
    except WebSocketClosedError:
      debug("WebSocket closed")
      continue
    except WebSocketProtocolMismatchError:
      debug("WebSocket used unknown protocol: ",
            getCurrentExceptionMsg())
      continue
    except WebSocketError:
      debug("WebSocket error: ", getCurrentExceptionMsg())
      continue

    if data.len() == 0:
      debug("ignoring empty WebSocket packet")
      continue

    if data.len() > 4 and data[0..3] == cast[seq[byte]]("RIFF"):
      result = createTempFile("radioio-from-zlic-", ".wav")[1]
      debug("downloading to ", result)
      writeFile(result, data)
      return result

    when defined(release):
      warn("ignoring unknown WebSocket packet")
    else:
      warn("ignoring unknown WebSocket packet: ", data.len())
