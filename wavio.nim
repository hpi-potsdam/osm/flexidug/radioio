## This module implements writing/reading to/from WAV files. It is a
## wrapper around `easywave <https://github.com/johnnovak/easywave>`__,
## a Nim library, again, for reading and writing WAV files.
##
## Fundamental working is open an file specified by its full path,
## writing or reading frames, and finally closing the file.
##
## .. [#il] interleaved sample format: ``ch1s1, ch2s1, ch1s2, ch2s2, …``

import std/logging
import std/enumerate

import easywave

import common


type
  WavIn* = object
    ## state for reading WAV files, represents an open WAV file
    reader*: RiffReader
      ## the reader instance to use for reading the WAV file
    format: WaveFormat
      ## format information of the opened WAV file
  WavOutSample = int16
    ## type alias for sample format to use for writing WAV files
  WavOut* = object
    ## state for writing WAV files, represents an open WAV file
    writer: RiffWriter
      ## the writer instance to use for reading the WAV file


var
  CHAR_BIT {.importc: "CHAR_BIT", header: "<limits.h>".}: int
    ## platform-specific number of bits per char

proc startWave*(fileName: string): WavOut =
  ## Writes a format chunk to a new WAV file stored under the full path
  ## specified via `fileName` and prepares to write to the data chunk.
  result.writer = createRiffFile(fileName, FourCC_WAVE)

  let format = WaveFormat(
    sampleFormat: sfPCM,
    bitsPerSample: sizeof(WavOutSample) * CHAR_BIT,
    sampleRate: FRAMES_PER_SEC,
    numChannels: NUM_CHANNELS
  )

  result.writer.writeFormatChunk(format)
  result.writer.beginChunk(FourCC_WAVE_data)


proc write*(wo: WavOut, sample: SampleValue) =
  ## Appends the single `sample` to the WAV file. Be aware that the
  ## samples of the channels need to be interleaved[#il]_.
  ## Converts to the target sample format aliased via WavOutSample_.
  const
    convCoeff = SampleValue(WavOutSample.high()) / SAMPLE_MAX_AMPLITUDE
  assert abs(sample) <= SAMPLE_MAX_AMPLITUDE
  wo.writer.write(WavOutSample(sample * convCoeff))


proc write*(wo: WavOut, frame: FrameData) =
  ## Appends `frame` to the WAV file via `write(WavOut, SampleValue)`_.
  for sample in frame:
    wo.write(sample)


proc finishWave*(wo: WavOut) =
  ## Closes the WAV file written to. Closing is important, e.g., to
  ## finish the data chunk of the file and adhere to the format.
  wo.writer.endChunk()
  wo.writer.close()


proc openWave*(fileName: string): WavIn =
  ## Opens a WAV file for reading, i.e., read format information and
  ## seek to the data chunk.
  result.reader = openRiffFile(fileName)

  discard result.reader.enterGroup()

  # according to WAV format, first chunk should be format chunk
  assert result.reader.currentChunk().id == FourCC_WAVE_fmt

  result.format = result.reader.readFormatChunk

  # seek to last chunk, which is the data chunk according to WAV format
  while result.reader.hasNextChunk():
    discard result.reader.nextChunk()
  assert result.reader.currentChunk().id == FourCC_WAVE_data


proc framesPerSec*(wi: WavIn): FrameCount =
  ## Returns the sample rate (i.e., frame rate) of the WAV file.
  return FrameCount(wi.format.sampleRate)


proc channelCount*(wi: WavIn): ChannelCount =
  ## Returns the number of channels in the WAV file.
  return ChannelCount(wi.format.numChannels)


proc frameCount*(wi: WavIn): FrameCount =
  ## Calculates and returns  the total number of frames in the WAV file.
  let chunkSize = int(wi.reader.currentChunk().size)
  let sampleBits  = int(wi.format.bitsPerSample)
  assert sampleBits mod 8 == 0
  let sampleBytes  = sampleBits div 8
  assert chunkSize mod sampleBytes == 0
  return uint(chunkSize / sampleBytes)


proc samplesPerChannel*(wi: WavIn): FrameCount =
  ## Calculates and returns
  ## the number of samples in each channel in the WAV file.
  let frameCount = int(wi.frameCount)
  let channelCount = int(wi.channelCount)
  assert frameCount mod channelCount == 0
  return uint(frameCount / channelCount)


proc length*(wi: WavIn): SecondValue =
  ## Calculates and returns the length of the WAV file in seconds.
  return int(wi.samplesPerChannel) / int(wi.format.sampleRate)


iterator samples*(wi: WavIn): SampleValue =
  ## Iterates over the samples in the WAV file. According to the WAV
  ## format, samples are interleaved[#il]_.

  template yieldAll(T: typedesc) =
    let convCoeff = SAMPLE_MAX_AMPLITUDE / SampleValue(T.high())
    let numSamples = wi.reader.currentChunk().size div uint32(sizeof(T))
    for _ in 0..<numSamples:
      yield convCoeff * SampleValue(wi.reader.read(T))

  case wi.format.sampleFormat
  of sfPCM:
    case wi.format.bitsPerSample
    of 8:
      yieldAll(int8)
    of 16:
      yieldAll(int16)
    of 32:
      yieldAll(int32)
    else:
      error(wi.reader.filename, " has unsupported bit depth: ",
            wi.format.bitsPerSample)
  of sfFloat:
    case wi.format.bitsPerSample
    of 32:
      yieldAll(float32)
    of 64:
      yieldAll(float64)
    else:
      error(wi.reader.filename, " has unsupported bit depth: ",
            wi.format.bitsPerSample)
  else:
    error(wi.reader.filename, " has unknown sample format.")


iterator frames*(wi: WavIn): FrameData =
  ## Iterates over the frames in the WAV file.
  var frame: FrameData
  for sampleNum, sample in enumerate(wi.samples):
    let channelNum = sampleNum mod int(wi.channelCount)

    if channelNum < NUM_CHANNELS:
      frame[channelNum] = sample
    # else: do nothing to skip sample from extra channels

    if channelNum + 1 == NUM_CHANNELS:
        yield frame
