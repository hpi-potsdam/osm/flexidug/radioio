FROM nimlang/nim

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update
RUN apt-get install -y pulseaudio
RUN apt-get install -y portaudio\*-dev libportaudio2

ENV PULSE_SERVER=tcp:host.docker.internal:4713

COPY *nim* /
RUN nimble install -yd
RUN nimble build
CMD build/radioio --sip-host host.docker.internal --sip-port 5007
