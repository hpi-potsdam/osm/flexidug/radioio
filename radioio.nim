## radio input/output for FlexiDug
## ===============================
##
## For a general description, development and deployment information
## of this program, please see `the project's README file
## <https://gitlab.com/hpi-potsdam/osm/flexidug/radioio>`__.
##
## This is the main module. It implements the top-most program
## coordination to glue radio hardware and the ZLiC together.
##
## sub modules
## -----------
##
## Interaction with sub modules is structured as follows::
##
##            ┌──────────────┐                            ︵︵︵︵︵
##            │    zlicio    │ ───    WebSocket    ───   (  ZLiC   )
##            └──────────────┘                            ︶︶︶︶︶
##                   │
##                   │
##            ╔══════════════╗     ┌─────────────┐
##   CLI  ─── ║   radioio    ║ ─── │    wavio    │
##            ╚══════════════╝     └─────────────┘
##                   │                    │
##                   │                    │
##            ┌──────────────┐
##            │   audioio    │      easywave lib.
##            └──────────────┘
##                   │                    │
##                   │                    │
##
##             NordAudio lib.         WAV files
##
##                   │                    │
##                   │                    │
##
##             PortAudio lib.        file system
##
##                   │
##                   │
##
##                OS audio     ───  audio hardware  ───  radio hardware
##
## Please follow the links below for more information on the sub
## modules:
##
## - `audioio <audioio.html>`__ –
##   implements input/output from/to audio hardware
## - `common <common.html>`__ –
##   implements common constants, types, and helpers
## - `framebuffer <framebuffer.html>`__ –
##   implements a queue-like buffer for recording/playing audio frames
## - `postprocessing <postprocessing.html>`__ –
##   implements buffer-based processing of audio
## - `preprocessing <preprocessing.html>`__ –
##   implements stream-based processing audio (i.e., filtering)
## - `wavio <wavio.html>`__ –
##   implements input/output from/to WAV files
## - `zlicio <zlicio.html>`__ –
##   implements connection from/to the server components of the ZLiC
##
## responsibilities
## ----------------
##
## This module is responsible for:
##
## - initialization of sub modules, i.e.:
##
##   - audio hardware input/output
##   - ZliC upload/download
##
## - process (e.g., filter) and – as appropriate – record audio input
## - post-process recorded audio
## - submit recorded audio for uploading to the ZliC
## - watch for audio downloaded from the ZliC
## - submit downloaded audio for playback
## - threading and thread coordination
## - setup of logging
##

import std/enumerate
import std/logging
import std/tempfiles

import asyncdispatch
import random
import os

import common
import audioio
import framebuffer
import preprocessing
import postprocessing
import wavio
import zlicio

import argparse

const
  LOOKAHEAD_MS = 100
    ## time of audio at beginning of a recording before the gate opened
  LOOKAHEAD_FRAMES = msToSamples(LOOKAHEAD_MS)
    ## converted `LOOKAHEAD_MS` for frame-based implementation

  HPF_FREQ = 150.0
    ## High-pass filter frequency (Hz), below which audio is filtered out.
  LPF_FREQ = 8_000.0
    ## Low-pass filter frequency (Hz), above which audio is filtered out.
  NEED_LPF = FRAMES_PER_SEC div 2 > LPF_FREQ # greetings from Nyquist
    ## Calculates if low-pass filter is needed (i.e., if the low-pass
    ## filter frequency is below half the sample rate)

  GATE_THRESH = -36.0
    ## DBFS above which the simple peak-tracking gate opens.
  GATE_HOLD_MS = 3_000.0
    ## amount of time the gate stays open after input below threshold

  REC_MIN_MS = LOOKAHEAD_MS + GATE_HOLD_MS + 500
    ## minimum length of a recording, shorter ones are discarded
  REC_MIN_FRAMES = msToSamples(REC_MIN_MS)
    ## converted `REC_MIN_FRAMES` for frame-based implementation
  REC_MAX_MS = 30_000
    ## maximum length of a recording, longer inputs start new recordings
  REC_MAX_FRAMES = msToSamples(REC_MAX_MS)
    ## converted `REC_MAX_FRAMES` for frame-based implementation

  IN_BUFFER_MS = (REC_MAX_MS + LOOKAHEAD_MS) * 1.1
    ## Length of audio in the (statically pre-allocated) recording buffer.
    # buffer has 10 % of maximum recording length as safety margin
  IN_BUFFER_FRAMES = msToSamples(IN_BUFFER_MS)
    ## converted `IN_BUFFER_MS` for frame-based implementation

  PLAY_PRE_NOISE_MS = 100
    ## length of noise to trigger VOX before playing audio from ZliC
  PLAY_PRE_SILENCE_MS = 500
    ## pause between noise and audio from ZliC

  PLAY_MIN_MS = 100
    ## minimum length of audio for playback, shorter files are discarded
  PLAY_MAX_MS = 30_000
    ## maximum length of audio for playback, longer files are discarded
  PLAY_MAX_FRAMES = msToSamples(REC_MAX_MS)
    ## converted `PLAY_MAX_FRAMES` for frame-based implementation

  OUT_BUFFER_FRAMES = PLAY_MAX_FRAMES
    ## number of frames the buffer for playing back audio can hold

type
  RadioioData = ref object
    inFrames: FrameBuffer
    ## buffer for filtered audio, possibly being send to the ZliC
    ai: AudioInData
    ## data structure for audio input module
    outWavs: Channel[string]
    ## a queue which detaches sending of audio from its recording
    outFrames: FrameBuffer
    ## buffer for audio from ZliC to play back
    zio: ZlicIoData
    ## data structure for module which communicates with the ZliC


proc setupThread() =
  ## Implements initialization common to all threads (e.g., logging).
  ##
  ## To be called by each thread after their start.

  var fmtStr = defaultFmtStr
  when not defined(release):
    fmtStr = "t" & $getThreadId() & " " & fmtStr

  var lvlThresh = lvlInfo
  when not defined(release):
    lvlThresh = lvlAll

  addHandler(
    newConsoleLogger(fmtStr=fmtStr, levelThreshold=lvlThresh)
  )


proc finalizeRecording(rio: RadioioData, count: FrameCount) =
  ## To be called when the audio recorded to the buffer should be kept.
  ##
  ## Post-processes audio, writes a WAV file and submits the file name
  ## for being send to the ZliC.

  normalize(rio.inFrames, count)

  let outWav = createTempFile("radioio-to-zlic-", ".wav")[1]
  debug("writing to ", outWav)
  var wio = startWave(outWav)
  for frame in rio.inFrames.dequeue(count):
    wio.write(frame)
  finishWave(wio)
  debug("written to ", outWav)

  if not rio.outWavs.trySend(outWav):
    warn("could not submit file name for post processing, ",
         "post processing is probably not fast enough")


proc discardRecording(rio: RadioioData, count: FrameCount) =
  ## To be called when the audio recorded to the buffer should be
  ## dropped.
  ##
  ## Discards the corresponding number of frames from the buffer.
  for _ in 1..count:
    discard rio.inFrames.dequeue()


proc listen(rio: RadioioData) {.thread.} =
  ## Main coordination of continuously reading the audio input buffers,
  ## filtering and deciding which passages to keep.
  ##
  ## In other words, it tries to detect and record voice for further
  ## processing/sending/etc.
  setupThread()

  var hpf: emaHpfData
  initEmaHpf(hpf, HPF_FREQ)

  when NEED_LPF:
    var lpf: emaLpfData
    initEmaLpf(lpf, LPF_FREQ)

  var gate: gateData
  initGate(gate, GATE_THRESH, GATE_HOLD_MS)

  var kept: FrameCount = 0

  info("listening on audio input")

  while true:

    rio.ai.ensureStreaming

    var available = rio.inFrames.used

    if kept == available:
      # Sleep for 1 % of buffer capacity. This "heuristic" is cheap and
      # sufficient. A previous implementation (expensively) tracked the
      # buffer size PortAudio uses internally to sleep for roughly one
      # buffer size. However, efficiency is of higher concern than low
      # latency here.
      const ms = int(IN_BUFFER_MS / 100)
      assert ms > 0
      sleep(ms)
      continue

    # process in batches, mainly to not call the expensively
    # `rio.ai.ensureStreaming` for every sample
    for _ in kept..<available:

      var frame = rio.inFrames[kept]

      filter(hpf, frame)

      when NEED_LPF:
        filter(lpf, frame)

      filter(gate, frame)

      rio.inFrames[kept] = frame

      if kept < LOOKAHEAD_FRAMES:
        kept += 1
        continue

      if unlikely(gate.isOpen()): # gate now open

        if unlikely(kept == LOOKAHEAD_FRAMES): # gate just opened
          info("starting audio recording")

        else: # gate has been open before
          assert kept > LOOKAHEAD_FRAMES

        kept += 1

        if unlikely(kept == REC_MAX_FRAMES):
          info("stopping audio recording (maximum length reached)")
          rio.finalizeRecording(kept)
          kept = 0

      else: # gate is now closed

        if likely(kept == LOOKAHEAD_FRAMES): # gate has been closed before
          discard rio.inFrames.dequeue()

        else: # gate just closed

          assert kept > LOOKAHEAD_FRAMES

          info("stopping audio recording (gate closed)")

          if likely(kept - LOOKAHEAD_FRAMES > REC_MIN_FRAMES):
            rio.finalizeRecording(kept)
          else:
            info("discarding audio recording (too short)")
            rio.discardRecording(kept)

          kept = 0


proc toZlic(rio: RadioioData) {.async.} =
  ## Main coordination of sending audio files to the ZliC.
  ##
  ## It reads WAV file names from a queue and sends them via Websocket.

  var
    dataAvailable: bool
    outWav: string

  info("waiting for audio messages for ZliC")

  while true:

    # xxx bonus: Get rid of polling. Problem is, that WebSocket rx and
    # tx must happen in same thread (library limitation, I guess).
    # Consequently, rx from WebSocket and rx from outWavs must happen
    # in same thread. There is no easy way for async rx from outWavs,
    # so how not to block rx from Websocket?
    (dataAvailable, outWav) = rio.outWavs.tryRecv()
    if likely(not dataAvailable):
      await sleepAsync(1_000)
      continue

    info("sending audio message to ZliC")

    rio.zio.upload(outWav)
    removeFile(outWav)

    info("sent audio message to ZliC")


proc getPreRollFrames(): FrameBuffer =
  ## Generates and returns audio which is played before playing back
  ## audio coming from the ZliC.

  ## This is currently used for triggering the voice-activated
  ## transmission (VOX) of (voice) radios with a short noise burst.

  let frameCount = msToSamples(PLAY_PRE_NOISE_MS + PLAY_PRE_SILENCE_MS)

  result = FrameBuffer()
  initFrameBuffer(result, frameCount)

  for _ in 0..<msToSamples(PLAY_PRE_NOISE_MS):
    var f: FrameData
    for c in 0..<NUM_CHANNELS:
      f[c] = rand(SAMPLE_MAX_AMPLITUDE * 2) - SAMPLE_MAX_AMPLITUDE
    result.enqueue(f)

  for _ in 0..<msToSamples(PLAY_PRE_SILENCE_MS):
    result.enqueue(FRAME_SILENCE)

  normalize(result, frameCount)


proc fromZlic(rio: RadioioData) {.async.} =
  ## Main coordination of receiving and playing audio from the ZliC.
  ##
  ## It waits for WAV files being downloaded, sanity-checks the audio,
  ## pre-processes and triggers the playback of the audio.

  var ao: AudioOutData
  initAudioOut(ao)

  let preRollFrames = getPreRollFrames()

  info("waiting for audio messages from ZliC")

  while true:
    let wavFileName = rio.zio.download
    let wavIn = openWave(wavFileName)

    if wavIn.channelCount != NUM_CHANNELS:
      warn("Audio from ZliC has unsupported number of channels. ",
           "Expected ", NUM_CHANNELS, " but got ", wavIn.channelCount)

    if wavIn.framesPerSec != FRAMES_PER_SEC:
      warn("Audio from ZliC has unsupported sample rate. ",
           "Expected ", FRAMES_PER_SEC, " but got ", wavIn.framesPerSec)

    block tryPlay:

      if (wavIn.length * 1_000) < PLAY_MIN_MS:
        error("Audio from ZliC too short. Discarding audio.")
        break tryPlay

      if wavIn.length * 1_000 > PLAY_MAX_MS:
        error("Audio from ZliC too long. Discarding audio.")
        break tryPlay

      if wavIn.samplesPerChannel > OUT_BUFFER_FRAMES:
        error("Audio from ZliC too large. Discarding audio. ",
              "Maybe sample rate of audio from ZliC too high?")
        break tryPlay

      assert rio.outFrames.empty

      for frameNum, frame in enumerate(wavIn.frames):
        assert not rio.outFrames.full
        rio.outFrames.enqueue(frame)

      normalize(rio.outFrames, rio.outFrames.used)

      info("playing audio message from ZliC")
      playFrames(ao, preRollFrames, FRAMES_PER_SEC)
      playFrames(ao, rio.outFrames, wavIn.framesPerSec)
      info("played audio message from ZliC")

    debug("clean up audio playback")
    rio.outFrames.clear()
    removeFile(wavFileName)


proc communicate(rio: RadioioData) {.thread.} =
  ## Main thread procedure for communication with the ZliC.
  ##
  ## Not much logic here, just triggers the procedures for sending data
  ## to and receiving data from the ZliC.
  setupThread()

  asyncCheck toZlic(rio)
  asyncCheck fromZlic(rio)


proc initRadioio(rio: RadioioData) =
  ## Sets up a ``RadioioData`` type by initializing the buffers.

  rio.inFrames = FrameBuffer()
  initFrameBuffer(rio.inFrames, IN_BUFFER_FRAMES)

  rio.outFrames = FrameBuffer()
  initFrameBuffer(rio.outFrames, OUT_BUFFER_FRAMES)


proc main() =
  ## First entry point, top-most program setup.
  ##
  ## Handles CLI, global variables, and setup of threads.
  setupThread()

  const
    threadProcs = [listen, communicate]

  var
    rio = RadioioData()
    threads: array[threadProcs.len, Thread[RadioioData]]

  initRadioio(rio)

  initAudioIn(rio.ai, rio.inFrames)

  # xxx: makeshift CLI args
  var parser = newParser:
    option("--sip-host", default=some("sip.zlic.lab.osmhpi.de"))
    option("--sip-port", default=some("80"))
    option("--sip-base-url", default=some("ws"))
  try:
    let opts = parser.parse()
    initZlicIo(rio.zio, opts.sipHost, opts.sipPort.parseInt, "ws")
  except ShortCircuit as err:
    if err.flag == "argparse_help":
      echo err.help
      quit(1)

  rio.outWavs.open()

  for t in 0..<threadProcs.len:
    createThread(threads[t], threadProcs[t], rio)

  joinThreads(threads)
  rio.outWavs.close()


main()
