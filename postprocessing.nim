## This module contains functionality to post-process audio.
##
## The implementations work on series of buffered frames.

import common
import framebuffer
import std/logging


proc normalize*(fb: FrameBuffer, count: FrameCount, limitDb = -1.0) =
  ## normalizes the first `count` frames in frame buffer `fb`
  debug("normalizing audio in buffer")

  var peak: SampleValue = SAMPLE_SILENCE

  # find current peak value
  for frame in fb.peek(count):
    for sample in frame:
      peak = max(peak, abs(sample))

  # calculate required gain
  let gain = dbfsToAmplitude(limitDb) / peak

  # apply gain
  for fi in frameIndices(count):
    fb[fi] = fb[fi] * gain
