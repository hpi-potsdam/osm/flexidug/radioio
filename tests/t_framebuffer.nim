import std/strformat
import std/unittest

import ../common
import ../framebuffer

suite "test framebuffer operations":

  setup:
    var fb = FrameBuffer()

  test "creating framebuffer of size zero should not work":
    expect AssertionDefect:
      initFrameBuffer(fb, 0)

  test "tests for framebuffer of non-zero size":
    for size in [1, 2, 3, 5]:
      for offset in 0..size+2:
        echo fmt"testing framebuffer of size {size}, ",
             fmt"head/tail offset {offset}"
        fb = FrameBuffer()
        initFrameBuffer(fb, FrameCount(size))

        # use the framebuffer, so head/tail not in initial positions
        # for all tests
        for _ in 0..offset:
          fb.enqueue(FRAME_SILENCE)
          discard fb.dequeue()

        doAssert fb.empty

        for _ in 1..<fb.capacity:
          doAssert not fb.full
          fb.enqueue(FRAME_SILENCE)
          doAssert not fb.empty

        doAssert not fb.full

        fb.enqueue(FRAME_SILENCE)

        doAssert fb.full

        for _ in 1..fb.capacity:
          doAssert not fb.empty
          discard fb.dequeue
          doAssert not fb.full

        doAssert fb.empty

        fb.enqueue(FRAME_SILENCE)
        fb.clear
        doAssert fb.empty
