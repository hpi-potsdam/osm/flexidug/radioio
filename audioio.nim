## This module is the glue between the application and Nordaudio,
## the nim wrapper around the audio framework PortAudio.
##
## It covers functionality to start and stop input and output streams,
## as well to receive frames from the input buffer and play back frames
## from an output buffer.

import std/logging
import atomics

import nordaudio

import common
import framebuffer


const
  OUT_BUFFER_FRAMES = 512
    ## Latency does not matter much for playback, so a large buffer is
    ## preferred to save CPU time.

  paNoError = 0
    ## symbol missing nordaudio, represents error-free return code
  paInputOverflow = 0x02
    ## symbol missing nordaudio, represents input overflow in status flags
  paFramesPerBufferUnspecified = cast[StreamFlags](0)
    ## symbol missing nordaudio,
    ## lets PortAudio choose number of frames per buffer


type
  AudioData* = object of RootObj
    ## state common to audio input and output
    params: StreamParameters
    ## configuration for the stream (input and/or output)
    stream: ptr Stream
    ## handle of the opened stream (input and/or output)
  AudioInData* = object of AudioData
    ## state specific to audio input
    expectedActive: bool
    ## expected state of the stream, used to detect crashed streams
    data: CallbackData
    ## see `CallbackData`
  CallbackData = ref object
    ## state shared with the audio input callback
    frames: FrameBuffer
    ## this is where the callback writes recorded audio to
    noFramesLost: AtomicFlag
    ## a flag which indicates if pieces of audio dropped out somewhere
  AudioOutData* = object of AudioData
    ## state specific to audio output
    buffer: seq[cfloat]
    ## buffer shared with PortAudio to hand over frames to play
    # the buffer could also held just in `playFrames` but we prefer to
    # pre-malloc the memory in `initAudioOut`


proc writeStream(stream: ptr Stream; buffer: pointer; frames: culong
  ): Error {.importc: "Pa_WriteStream", header: "portaudio.h", cdecl.}
  ## extends nordaudio wrapper, triggers PortAudio to play back buffer


proc bufferSeek[T](p: ptr T, off: int): ptr T =
  ## math to advance pointer `p` by `off` samples
  cast[ptr type(p[])](cast[ByteAddress](p) +% off * sizeof(p[]))


proc initAudioCommon(aio: var AudioData) =
  ## sets up state common to audio input and output
  if initialize() != paNoError:
    error("could not initialize audio back end PortAudio")
    return

  aio.params.channelCount = NUM_CHANNELS
  aio.params.sampleFormat = paFloat32


proc initAudioIn*(ai: var AudioInData, fb: FrameBuffer) =
  ## sets up state common to audio input
  initAudioCommon(ai)

  ai.params.device = getDefaultInputDevice()
  if ai.params.device == -1:
    error("no default audio input device")
    quit(1)

  ai.data = CallbackData()

  ai.data.frames = fb

  discard ai.data.noFramesLost.testAndSet

  ai.expectedActive = false


proc streamCallback(inBuffer: pointer;
                    outBuffer: pointer;
                    framesPerBuffer: culong;
                    timeInfo: ptr StreamCallbackTimeInfo;
                    statusFlags: StreamCallbackFlags;
                    userData: pointer
                   ): cint {.cdecl.} =
  ## Callback called by PortAudio to move data from PortAudio buffers
  ## to the buffers of this program.
  var
    data = cast[ptr CallbackData](userData)
    input = cast[ptr cfloat](inBuffer)
    framesPerBuffer = cast[uint](framesPerBuffer)
    inFrame: FrameData

  if unlikely(statusFlags != paNoError):
    # handle only cases where this callback has probably been too slow
    if bool(statusFlags and paInputOverflow):
      # PortAudio discarded input samples
      data.noFramesLost.clear

  if likely(input != nil):
    for frameNum in 0..<framesPerBuffer:
      for sampleNum in 0..<NUM_CHANNELS:
        inFrame[sampleNum] = input[]
        input = bufferSeek(input, 1)
      data.frames.enqueue(inFrame)

  cast[cint](paContinue)


proc stop(ai: var AudioInData) =
  ## tears down audio input stream
  debug("stopping audio input stream")
  discard closeStream(ai.stream)
  ai.expectedActive = false


proc start(ai: var AudioInData) =
  ## Begins audio input.
  ##
  ## Should only be called if stream is not active, unexpectedly or not.

  if ai.expectedActive:
    warn("audio input stream deactivated unexpectedly, will restart")
    stop(ai)

  debug("opening audio input stream")
  if openStream(
    addr(ai.stream),
    addr(ai.params),
    nil, # output parameters
    FRAMES_PER_SEC,
    paFramesPerBufferUnspecified,
    paClipOff and paNeverDropInput,
    streamCallback,
    addr(ai.data)
  ) !=  0:
    error("could not open stream for audio recording")
    quit(1)

  debug("starting audio input stream")
  discard startStream(ai.stream)

  ai.expectedActive = true


proc ensureStreaming*(ai: var AudioInData) =
  ## Check stream is active and healthy (i.e., no lost frames reported).
  ##
  ## This procedure is idempotent and can be called regularly.
  if unlikely(not ai.data.noFramesLost.testAndSet):
    error("lost input frames, probably not processing fast enough")

  if unlikely(isStreamActive(ai.stream) != 1):
    start(ai)


proc initAudioOut*(ao: var AudioOutData) =
  ## sets up state for audio output
  initAudioCommon(ao)

  ao.params.channelCount = NUM_CHANNELS
  ao.params.sampleFormat = paFloat32
  ao.params.device = getDefaultOutputDevice()
  if ao.params.device == -1:
    error("no default audio output device")

  ao.buffer = newSeq[cfloat](OUT_BUFFER_FRAMES * NUM_CHANNELS)


proc stop(ao: var AudioOutData) =
  ## tears down audio output stream
  debug("stopping audio output stream")
  discard closeStream(ao.stream)


proc start(ao: var AudioOutData, framesPerSec: FrameCount) =
  ## Begins audio output stream.
  ##
  ## Should only be called if stream is not active.

  debug("opening audio output stream")
  if openStream(
    addr(ao.stream),
    nil, # input parameters
    addr(ao.params),
    cdouble(framesPerSec),
    OUT_BUFFER_FRAMES,
    paClipOff,
    nil, # stream callback
    nil # stream callback data
  ) !=  0:
    error("could not open stream for audio playback")
    return

  debug("starting audio output stream")
  if startStream(ao.stream) != paNoError:
    error("could not start stream for audio playback")
    return


proc playFrames*(ao: var AudioOutData, fb: FrameBuffer,
                 framesPerSec: FrameCount) =
  ## Play frames from the buffer, for which an output stream is opened
  ## and closed.
  start(ao, framesPerSec)

  assert OUT_BUFFER_FRAMES mod NUM_CHANNELS == 0

  var framesInBuffer: uint = 0

  # Due to the internal ring-buffer like semantics of the FrameBuffer,
  # we cannot play from `fb` directly, unfortunately.
  # TODO: But maybe we can add a `playFrames` that works on an iterator
  # over FrameData? https://forum.nim-lang.org/t/7020#44084

  template flush() =
    var status: Error = writeStream(
      ao.stream, addr(ao.buffer[0]), culong(framesInBuffer)
    )
    if status != paNoError:
      error("lost output frames")
      debug("portaudio output error code: ", status)
    framesInBuffer = 0

  for frame in fb.peek(fb.used()):
    for sample in frame:
      ao.buffer[framesInBuffer] = sample
      framesInBuffer += 1
    if framesInBuffer == OUT_BUFFER_FRAMES:
      flush()

  if framesInBuffer > 0:
    for i in framesInBuffer..<OUT_BUFFER_FRAMES:
      ao.buffer[i] = SAMPLE_SILENCE
    flush()

  stop(ao)
