## a buffer for audio frames
## =========================
##
## Unfortunately, it seemed necessary to implement a custom data
## structure for audio frames this program has to buffer. Here it is.
##
## Requirements:
##
## - pre-allocation (implies static and bounded size)
## - queue semantics
## - allowing random reads and writes
## - thread-safe between enqueue and dequeue operations
## - of course, efficiency enqueue, random reads, and dequeue operations
##   (i.e., the hot operations in this program)
##
## There are limitations regarding general use. However, those
## limitations are of negligible relevance for this program:
## - for efficiency, bounds are not checked in release builds (asserts);
##   because a) calling code must often deal with such situations
##   anyway and b) even calling code can often do not much when too slow
## - again, no safety checks in release builds, over- and underruns
##   cause undefined behavior
## - single thread that enqueues
## - single thread that dequeues
##
## If there are existing projects which can be used, suggestions are
## welcome. Existing projects which were not used and why:
## - built-in channels: do not allow random reads
## - `RingBuffer.nim <https://github.com/megawac/RingBuffer.nim>`__
##   because it is not thread-safe and thus would required
##   coarse-grained locking.
## - `lockfreequeues <https://elijahr.github.io/lockfreequeues/>`__
##   because a) is only supports constant size and b) it causes a
##   segmentation fault with a size somewhere between 300 k and 320 k.

import atomics

import common


const CacheLineBytes* {.intdefine.} =
  when defined(powerpc):
    128
  else:
    64
  ## Platform-specific size of a cache line; used to align hot data to
  ## avoid thrashing.
  # 128 bytes on PowerPC, 64 bytes elsewhere

type FrameBuffer* = ref object
  ## A queue-like buffer for audio frames. See module description for
  ## general possibilities and limitations.

  capacity*: FrameCount
  ## Number of frames this buffer can hold.

  frames: seq[FrameData]
  ## Buffer data, i.e., audio frames.
  # To be able to differentiate between an empty and a full buffer, this
  # is one element larger than the requested capacity. An alternative
  # implementation would be to track the number of items in the buffer.
  #
  # Although they would be much more convenient to program, not using
  # std/lists here, because those require approx 5 times the memory.

  head {.align: CacheLineBytes.}: Atomic[FrameIndex]
  ## next index to read from

  tail {.align: CacheLineBytes.}: Atomic[FrameIndex]
  ## next index to write to


proc initFrameBuffer*(fb: FrameBuffer, capacity: FrameCount) =
  ## Set up `fb`, incl. memory allocation for frames.
  assert capacity > 0
  fb.capacity = capacity
  fb.frames = newSeq[FrameData](capacity + 1)
  for i in 0..fb.capacity:
    fb.frames[i] = FRAME_SILENCE
  fb.tail.store(0)
  fb.head.store(0)


proc wrapIndex(fb: FrameBuffer, fi: FrameIndex): FrameIndex =
  ## Wraps frame index `fi` around if required.
  ##
  ## `fi` MUST be smaller than two times the capacity of `fb`.
  # hot: called, e.g., for every peek
  assert fi < 2 * fb.capacity
  result = fi
  let framesLen = uint(fb.frames.len)
  if result >= framesLen:
    result -= framesLen


proc nextIndex(fb: FrameBuffer, fi: FrameIndex): FrameIndex =
  ## Increments frame index `fi` by one and wraps it around if required.
  result = fi + 1
  let framesLen = uint(fb.frames.len)
  if unlikely(result == framesLen):
    result -= framesLen
  assert result <= framesLen


proc empty*(fb: FrameBuffer): bool =
  ## Returns `true` if there is no frame in buffer `fb`,
  ## `false` otherwise.
  result = fb.tail.load() == fb.head.load()


proc used*(fb: FrameBuffer): FrameCount =
  ## Returns the number of frames in the buffer `fb`.
  result = fb.tail.load()
  let head = fb.head.load()
  if unlikely(head > result):
    result += fb.capacity
  result -= head


proc full*(fb: FrameBuffer): bool =
  ## Returns `true` if no more frames can be stored in buffer `fb`,
  ## `false` otherwise.
  result = fb.nextIndex(fb.tail.load()) == fb.head.load()


proc enqueue*(fb: FrameBuffer, fd: FrameData) =
  ## Adds frame `fd` to the end of buffer `fb`.
  ##
  ## There *must* be space for one frame in the buffer.
  assert not fb.full
  let tail = fb.tail.load()
  fb.frames[tail] = fd
  fb.tail.store(fb.nextIndex(tail))


proc dequeue*(fb: FrameBuffer): FrameData =
  ## Removes and returns one frame from the buffer `fb`.
  ##
  ## There *must* be a frame in the buffer.
  # Note that the tail must not be modified here.
  assert not fb.empty
  let head = fb.head.load()
  result = fb.frames[head]
  fb.head.store(fb.nextIndex(head))


iterator dequeue*(fb: FrameBuffer, count: FrameCount): FrameData =
  ## Removes and returns `count` frames from the buffer `fb`.
  ##
  ## There *must* be at least `count` frames in the buffer.
  assert count <= fb.used
  var index = fb.head.load()
  for fi in 0..<count:
    yield fb.frames[index]
    index = fb.nextIndex(index)
  fb.head.store(index)


proc clear*(fb: FrameBuffer) =
  ## Removes all frames from the buffer `fb`.
  fb.head.store(fb.tail.load())


proc `[]`*(fb: FrameBuffer, fi: FrameIndex): FrameData =
  ## Returns the `fi`th frame from the buffer `fb` without removing it.
  ##
  ## There *must* be more than `fi` frames in the buffer.
  assert fi < fb.used
  result = fb.frames[fb.wrapIndex(fb.head.load() + fi)]


proc `[]=`*(fb: FrameBuffer, fi: FrameIndex, fd: FrameData) =
  ## Replaces `fi`th frame in the buffer `fb`.
  ##
  ## There *must* be more than `fi` frames in the buffer.
  assert fi < fb.used
  fb.frames[fb.wrapIndex(fb.head.load() + fi)] = fd


iterator peek*(fb: FrameBuffer; count: FrameCount): FrameData =
  ## Iterate over the `count` oldest frames in the frame buffer `fb`.
  ##
  ## There *must* be at least `count` frames in the buffer.
  assert count <= fb.used
  var index = fb.head.load()
  for fi in 0..<count:
    yield fb.frames[index]
    index = fb.nextIndex(index)
