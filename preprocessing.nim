## This module contains functionality to pre-process/filter audio.
##
## The implementations are stateful and work in-place on single frames.

import math

import common


proc hzToAlpha(hz: float): float =
  ## calculates alpha for EMA filter by cutoff frequency
  ##
  ## basic formula:
  ## https://tttapa.github.io/Pages/Mathematics/Systems-and-Control-Theory/Digital-filters/Exponential%20Moving%20Average/Exponential-Moving-Average.html#cutoff-frequency
  ## solved by frequency:
  ## https://www.wolframalpha.com/input?i=solve%28h%3D%28f%2F%282*p%29%29+*+arccos%28%28a%C2%B2%2B2a-2%29%2F%282a-2%29%29%2Ca%29
  var cos2hpDivS = cos(2.0 * hz * PI / FRAMES_PER_SEC)
  return sqrt(pow(cos2hpDivS, 2) - 4.0 * cos2hpDivS + 3) +
         cos2hpDivS - 1


type
  singleStageEmaLpfData = object
    ## Instance state for a single-stage EMA LPF filter.

    alpha: float
    ## Determines the cutoff frequency (depends on the sample rate).

    previousFrameOut: FrameData
    ## Stores previous output frame; needed to calculate the moving
    ## average.


proc initEmaLpf(lpf: var singleStageEmaLpfData, hz: float) =
  ## Initializes the Low-pass Filter `lpf` at frequency `hz`.
  ##
  ## Algorithm is single-stage Exponential Moving Average (EMA).
  lpf.alpha = hzToAlpha(hz)


proc filter(lpf: var singleStageEmaLpfData, frame: var FrameData) =
  ## Applies `lpf` to `frame` in-place.
  for ch in 0..<frame.len:
    frame[ch] = lpf.alpha * frame[ch] +
                (1 - lpf.alpha) * lpf.previousFrameOut[ch]
    lpf.previousFrameOut = frame


type emaLpfData* = object
  ## Instance state for a multi-stage EMA LPF filter.

  stages: seq[singleStageEmaLpfData]
  ## Number of stages this filter has. The more, the steeper the filter
  ## curve.


proc initEmaLpf*(lpf: var emaLpfData, hz: float, stages=5) =
  ## Initializes the Low-pass Filter `lpf` at frequency `hz`.
  ## The more `stages`, the steeper the filter curve.
  ##
  ## Algorithm is multi-stage Exponential Moving Average (EMA).
  lpf.stages = newSeq[singleStageEmaLpfData](stages)
  for s in 0..<lpf.stages.len:
    initEmaLpf(lpf.stages[s], hz)


proc filter*(lpf: var emaLpfData, frame: var FrameData) =
  ## Applies `lpf` to `frame` in-place.
  for s in 0..<lpf.stages.len:
    filter(lpf.stages[s], frame)


type emaHpfData* = object
  ## Instance state for a multi-stage EMA HPF filter.

  stages: seq[singleStageEmaLpfData]


proc initEmaHpf*(hpf: var emaHpfData, hz: float, stages=5) =
  ## Initializes the High-pass Filter `hpf` at frequency `hz`.
  ## The more `stages`, the steeper the filter curve.
  ##
  ## Algorithm is multi-stage Exponential Moving Average (EMA).
  hpf.stages = newSeq[singleStageEmaLpfData](stages)
  for s in 0..<hpf.stages.len:
    initEmaLpf(hpf.stages[s], hz)


proc filter*(hpf: var emaHpfData, frame: var FrameData) =
  ## Applies `hpf` to `frame` in-place.
  for s in 0..<hpf.stages.len:
    var lpfFrame: FrameData = frame
    filter(hpf.stages[s], lpfFrame)
    frame -= lpfFrame


type gateData* = object
  ## Instance state for a noise gate.

  threshold: float
  ## Amplitude (full-scale) above which the gate opens.

  hold: FrameCount
  ## Number of samples the gate will at least remain open.

  holdCounter: FrameCount
  ## Countdown of samples the gate will remain open.
  ## Reset every time the input signal is above the threshold.
  ## Indicates if the gate is currently open (`> 0`) or closed (`0`).


proc initGate*(gate: var gateData, thresholdDbFS: float,
               holdMs: float) =
  ## Initializes the noise `gate` with `thresholdDbFS`, and `holdMs`.
  ## This is a hard gate, i.e., the output is silent when
  ## closed.
  ##
  ## Note that the implementation does not feature parameters for attack
  ## (gain ramp when opening) or release (gain ramp when closing), since
  ## this is not required for the Radio I/O use case for FlexiDug.
  gate.threshold = dbfsToAmplitude(thresholdDbFS)
  gate.hold = msToSamples(holdMs)


proc isOpen*(gate: gateData): bool =
  ## Returns if the `gate` is currently open or closed (Boolean).
  gate.holdCounter > 0


proc filter*(gate: var gateData, frame: var FrameData) =
  ## Applies `gate` to `frame` in-place.

  assert gate.holdCounter >= 0
  if unlikely(gate.isOpen()):
    gate.holdCounter -= 1

  for sample in frame:
    if unlikely(sample >= gate.threshold):
      gate.holdCounter = gate.hold
      continue
