version = "0.1.0"
author = "Lukas Pirl"
description = "radio in/out module for FlexiDug"
license = "GPLv3"

binDir = "build"
bin = @["radioio"]

requires "https://github.com/johnnovak/easywave"
requires "nordaudio"
requires "ws"
requires "uuid4"
requires "argparse"

task release, "creates release build":
  exec "nim c --define:release radioio"

task docs, "creates project documentation":
  let token = "to-be-replaced.example.com"
  let commit = "main" # gorge "git rev-parse --short HEAD"

  exec(
    "nim doc --git.url:'" & token & "' --git.commit:'" & commit & "' " &
    "--git.devel:main --project --index:on --docInternal " &
    "--outdir:docs radioio"
  )

  # URL generation is not yet compatible to GitLab, let sed fix it:
  exec(
    r"find docs -name '*.html' -print0 | xargs -rt0 " &
    r"sed -ie 's|to-be-replaced.example.com/|" &
    r"https://gitlab.com/hpi-potsdam/osm/flexidug/radioio/-/|g'"
  )

task clean, "removes build artifacts":
  rmDir "build"
  rmDir "docs"
